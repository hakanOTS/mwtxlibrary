package at.orangetechsoft.javalibrary.otsmwtxlibrary.activities;

import android.app.Activity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import at.orangetechsoft.javalibrary.otsmwtxlibrary.helpers.OTSMWLIBHelper;

public class OTSMWLIBAdvancedSettingsActivity {

    private CheckBox cb_notification_vibration, cb_notification_sound;
    private Activity activity;
    private View backbutton;

    public OTSMWLIBAdvancedSettingsActivity(Activity activity, CheckBox vibrationCb, CheckBox soundCb, View backbutton) {
        this.activity = activity;
        this.backbutton = backbutton;
        cb_notification_vibration = vibrationCb;
        cb_notification_sound = soundCb;
        initialise();
    }

    private void initialise() {
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onBackPressed();
            }
        });
        String vibrate = OTSMWLIBHelper.readFromSharedPref(activity, "vibrate");
        String sound = OTSMWLIBHelper.readFromSharedPref(activity, "sound");
        cb_notification_vibration.setChecked((vibrate != null && vibrate.equals("true")));
        cb_notification_sound.setChecked((sound != null && sound.equals("true")));
        cb_notification_vibration.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                OTSMWLIBHelper.writeToSharedPref(activity, "vibrate", isChecked ? "true" : "false");
            }
        });
        cb_notification_sound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                OTSMWLIBHelper.writeToSharedPref(activity, "sound", isChecked ? "true" : "false");
            }
        });
    }
}
