package at.orangetechsoft.javalibrary.otsmwtxlibrary.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;


public class OTSMWLIBAboutAppActivity {

    private View ots, back, firm;
    private TextView tw_version;
    private Activity activity;
    private String otswebsite, customerwebsite, about_app_info;

    public OTSMWLIBAboutAppActivity(Activity activity, View ots, View back, View firm, TextView tw_version,
                                    String otswebsite, String customerwebsite, String about_app_info) {
        this.ots = ots;
        this.back = back;
        this.firm = firm;
        this.tw_version = tw_version;
        this.activity = activity;
        this.otswebsite = otswebsite;
        this.customerwebsite = customerwebsite;
        this.about_app_info = about_app_info;
        initialise();
    }

    private void initialise() {
        ots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(otswebsite));
                activity.startActivity(browserIntent);
            }
        });
        firm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(customerwebsite));
                activity.startActivity(browserIntent);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });

        String version = "";
        try {
            PackageInfo pInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String infotxt = about_app_info + " " + version;
        tw_version.setText(infotxt);
    }
}
