package at.orangetechsoft.javalibrary.otsmwtxlibrary.helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class OTSMWLIBHelper {

    public static void writeToSharedPref(Context context, String key, String value) {

        SharedPreferences prefs = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        prefs.edit().putString(key, value).apply();
    }

    public static String readFromSharedPref(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        return prefs.getString(key, null);
    }
}
